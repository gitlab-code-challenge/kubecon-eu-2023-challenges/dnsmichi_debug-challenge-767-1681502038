# KubeCon/CloudNativeCon EU 2023 Code Challenge

## Getting started

Welcome to the KubeCon/CloudNativeCon EU 2023 Code Challenge hosted on CodeChallenge.dev. _We're so glad you're here_.

Fork this project into your namespace to get started.

## Level 1

The `.gitlab-ci.yml` in this project uses the `alpine:latest` image. This image is often used, and we want to reduce the number of direct pulls from Docker Hub by using the [GitLab Dependency Proxy](https://docs.gitlab.com/ee/user/packages/dependency_proxy/). For this level, you will be changing the CI/CD configuration to use the dependency proxy instead of pulling from upstream. To do that, you need to prefix the predefined variable `CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX` to the container image as shown below:

```yaml
image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/alpine:latest
```

Create a merge request with your changes and this level will be marked as complete when the predefined variable `CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX` is detected in your MR changes.

## Level 2

For level 2, this project provides an Image-to-ASCII art converter in Golang. No worries, there is no deep Golang knowledge required. For some reason, the tests fail and block the CI/CD pipeline deployment. Tip: Someone forgot to update the challenge from last year in `main.go`.

Fix the wrong value in the `helloFrom` variable, commit the changes and inspect the `deploy` job in the CI/CD pipeline. The CI/CD jobs will also generate [SLSA-2 attestation](https://about.gitlab.com/blog/2022/11/30/achieve-slsa-level-2-compliance-with-gitlab/) to help achieve SLSA Level 2 compliance with GitLab. Navigate into the `build` job details, and browse the artifacts to inspect the `artifacts-metadata.json` SLSA attestation file.

As a bonus exercise, change the `IMAGE_PATH` variable in `.gitlab-ci.yml` to the proposed value, and see what happens.

The level will be completed once the corrected value will be detected in `main.go` in the MR changes.

## Level 3

For level 3, visit the [Contributing to GitLab](https://about.gitlab.com/community/contribute/) page to get started. There are many things to contribute to:

* The Ruby on Rails backend
* The VueJS-based frontend
* The Go-based services like the GitLab Runner and Gitaly
* The Go-based GitLab CLI
* The Terraform provider for GitLab
* The cloud-native installation (Operator, Helm charts)
* The documentation for everything
* Translations
* And more!

You can also participate in one of our upcoming [Hackathons](https://about.gitlab.com/community/hackathon/#sessions).
